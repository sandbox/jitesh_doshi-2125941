; This file will download the projects used by this site
; Run it as ...
; drush make --no-core --contrib-destination=. drush.make .

core = 7.x
api = 2

libraries[stripe][download][type] = "git"
libraries[stripe][download][url] = "https://github.com/stripe/stripe-php.git"
libraries[stripe][directory_name] = stripe
libraries[stripe][destination] = apps/restng-demo
