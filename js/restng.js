// an AngularJS module to allow the restng app to fit inside Drupal pages
angular.module('restng', [])
.config(function($httpProvider) {
  if(typeof(Drupal.settings.restng.jwt_token) != 'undefined') {
    $httpProvider.defaults.headers.common['Authorization'] = Drupal.settings.restng.jwt_token;
  }
  // provider.defaults.headers.common['X-CSRF-SessionId'] = Drupal.settings.angularjsApp.restws_session_id;
  $httpProvider.interceptors.push(function($q, $log, $templateCache) {
    // this interceptor adds a prefix to all AJAX requests coming from Angular
    return {
      'request': function(config) {
        // activate only if restng base_href is defined
        if(typeof(Drupal.settings.restng.base_href) != 'undefined') {
          // do not prefix if the URL is in $templateCache or is absolute (starts with "/")
          if(!$templateCache.get(config.url) && config.url.slice(0, 1) !== "/") {
            config.url = Drupal.settings.restng.base_href + config.url;
          }
        }
        return config;
      }
    };
  });
})
;
