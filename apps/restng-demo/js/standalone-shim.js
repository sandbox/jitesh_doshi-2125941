// This file provides alternative versions of the JavaScript code
// that allows this app to be work standalone, in the absence of
// Drupal or other container application.

// create the 'restng' module empty to prevent JS errors
angular.module('restng', []);

// inject values and configuration into the main app module
angular.module('restng-demo')
.config(function($routeProvider) {
  // provide '/nodes' route
  $routeProvider.when('/nodes', {
    templateUrl: 'view/nodes.html'
  });
  // make '/nodes' the default route
  $routeProvider.otherwise({redirectTo: '/nodes'});
})
;
