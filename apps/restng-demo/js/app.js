(function() { // IIAF: begin

angular.module('restng-demo', ['restng', 'ngRoute', 'ngStorage'])
.config(function($routeProvider) {
  $routeProvider.when('/product', {
    templateUrl: 'view/product.html',
    controller: 'ProductCtrl as product'
  });
  $routeProvider.when('/checkout', {
    templateUrl: 'view/checkout.html'
  });
  $routeProvider.otherwise({redirectTo: '/product'});
})
.controller('AppCtrl', function($localStorage, AlertService) {
  var app = this;
  this.alert = AlertService;
  this.storage = $localStorage.$default({
    items: []
  });
  this.cart = {
    items: this.storage.items
  };
  this.cart.add = function(product) {
    this.items.push(product);
    this.computeTotal();
    AlertService.add('Cart item added.');
  };
  this.cart.empty = function() {
    this.items.length = 0;
    this.computeTotal();
    AlertService.add('Cart emptied.');
  };
  this.cart.remove = function(item) {
    var index;
    if(typeof item == 'number') {
      index = item;
    } else {
      index = this.items.indexOf(item);
    }
    this.items.splice(index, 1);
    this.computeTotal();
    AlertService.add('Cart item removed.');
  };
  this.cart.computeTotal = function() {
    this.total = 0;
    this.description = 'Product IDs';
    for(var i in this.items) {
      this.total += this.items[i].price;
      this.description += ':' + this.items[i].id;
    }
  };

  this.CheckoutCtrl = function($scope, $http, $log, stripe_publishable_key) {
    var cart = this.cart = app.cart;
    this.cart.computeTotal();
    this.payment = {
      amount: this.cart.total
    };
    Stripe.setPublishableKey(stripe_publishable_key);
    this.charge = function(cart, payment) {
      $log.debug('cart', cart);
      $log.debug('payment', payment);
      Stripe.card.createToken(payment, function(status, response) {
        $log.debug("response", response);
        if(response.id) {
          $scope.$apply(function() {
            cart.token = response.id; // WARNING: error checking omitted
            $http.post('charge.php', cart).success(function() {
              AlertService.add('Payment charged successfully.', 'success');
              app.cart.empty();
            }).error(function() {
              AlertService.add('Payment charge failed.', 'danger', 0);
            });
          });
        } else {
          AlertService.add('Failed to generate payment token.', 'danger', 0);
        }
      });
    }
  }

  this.cart.computeTotal();
})
.controller('ProductCtrl', function($http, nid) {
  var vm = this;
  $http.get("/node/" + nid + ".json").success(function(data) {
    vm.product = node2product(data);
  });
})
.controller('NodesCtrl', function($http) {
  var vm = this;
  $http.get('/node.json').success(function(data) {
    vm.products = [];
    for(var i in data.list) {
      vm.products.push(node2product(data.list[i]));
    }
  });
})
.factory('AlertService', function($timeout) {
  var service = {}
  service.items = [];
  service.add = function(message, type, timeout) {
    if(type === undefined) {
      type = 'info'; // types are 'success', 'info', 'warning', 'danger'
    }
    if(timeout === undefined) {
      timeout = 5000; // default timeout of 5sec
    }
    var obj = {message: message, type: type};
    this.items.push(obj);
    if(timeout) {
      $timeout(function() {
        var index = service.items.indexOf(obj);
        if (index >= 0) {
          service.remove(index);
        }
      }, timeout);
    }
  };
  service.remove = function(index) {
    this.items.splice(index, 1);
  };
  return service;
})
;

// utility function to convert node into product
function node2product(node) {
  return {
    id: node.nid,
    name: node.title,
    price: parseFloat(node.field_price)
  };
}

})(); // IIAF: end
