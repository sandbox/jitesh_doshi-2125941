<?php

require_once('stripe/init.php');
require_once('settings.inc');

_jwt_validation();

Stripe\Stripe::setApiKey($conf['stripe_secret_key']);

$request_body = file_get_contents('php://input');
$request = json_decode($request_body);

if(empty($request) || empty($request->total) || empty($request->token)) {
  _json_fail(401, "Bad request");
}

$charge['amount'] = (int)($request->total * 100);
$charge['card'] = $request->token;
$charge['currency'] = 'usd';
$charge['description'] = $request->description;

$response = Stripe\Charge::create($charge);

header('Content-Type: application/json');
print json_encode($response);

function _jwt_validation() {
  global $conf;
  if(!isset($conf['jwt_secret_key'])) {
    // no security check
    return;
  }

  if(!isset($_SERVER['HTTP_AUTHORIZATION'])) {
    _json_fail(401, "Missing JWT token in 'Authorization' header");
  }

  $jwt = _jwt_decode_token($_SERVER['HTTP_AUTHORIZATION']);
  // do more security checks here, and return if they pass
}

function _jwt_decode_token($encoded) {
  global $conf;
  if(isset($conf['jwt_secrety_key'])) {
    $path = $conf['jwt_lib_path'];
    include_once $path . "/JWT.php";
    $secret = $conf['jwt_secret_key'];
    $decoded = JWT::decode($encoded, $secret);
    return $decoded;
  } else {
    // no security check
    return TRUE;
  }
}

function _json_fail($response_code, $message) {
  http_response_code($response_code);
  header('Content-Type: application/json');
  print json_encode(array('message' => $message));
  exit(1);
}
