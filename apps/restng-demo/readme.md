AngularJS app to demo the restng framework
------------------------------------------

Before you begin, do the following:

- Download and unzip the Stripe PHP SDK into the ./stripe folder with 
  `git clone https://github.com/stripe/stripe-php.git stripe`
- Copy settings.inc.example to settings.inc and edit it to taste.
- Copy js/config.js.example to settings.inc and edit it to taste.
- In Drupal, enable the 'restng' module
- In Drupal, visit 'admin/structure/block/manage/restng/restng/configure'
- Make sure the block appears where you want
- Enter the following settings ...

ng-app:
restng-demo

block body:
<div ng-app="@restng_ngapp@">
  <div ng-controller="AppCtrl as app">
    <!-- alerts -->
    <div ng-repeat="a in app.alert.items" role="alert" class="alert alert-{{a.type}}">{{a.message}} <span ng-click="app.alert.remove($index)" class="glyphicon glyphicon-remove pull-right"></span></div>
    <div ng-view>Loading ...</div>
  </div>
</div>

external js files:
https://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js
https://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.min.js
https://rawgithub.com/gsklee/ngStorage/master/ngStorage.js
https://js.stripe.com/v2/

external CSS files:

app js files:
js/app.js

app css files:
css/app.css

custom js code (replace the key below with yours):
angular.module('restng-demo')
.value('nid', Drupal.settings.restng.nid)
.value('stripe_publishable_key', 'pk_test_...')
;
